# controls

#### 介绍
这是一个可以自由上传，使用修改控制器的maya插件

#### 使用视频
[https://www.bilibili.com/video/av76324668](https://www.bilibili.com/video/av76324668)

#### BUG反馈
[https://afdian.net/group/e268edc45df411eaa59b52540025c377](https://afdian.net/group/e268edc45df411eaa59b52540025c377)

#### 开发文档
[http://lush_ma.gitee.io/controls](http://lush_ma.gitee.io/controls)

#### 开发教程
[https://www.aboutcg.org/courseDetails/747/introduce](https://www.aboutcg.org/courseDetails/747/introduce)
